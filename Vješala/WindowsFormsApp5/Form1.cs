﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp5
{   
    public partial class Form1 : Form
    {
        string nasumicni = "";
        int broj_pokusaja = 10;


        class Pojmovi
        {
            #region data_memebers
            private string ime;
            #endregion

            #region public_methods
            public Pojmovi()
            {
                ime = "Vjesala";
            }

            public Pojmovi(string rijec)
            {
                ime = rijec;
            }

            public override string ToString()
            {
                return ime.ToString();
            }

            #endregion
        }
            List<Pojmovi> listaPojmova = new List<Pojmovi>();
            string path = "D:\\data.txt";
        static Random rnd = new Random();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Učitavanje pasa iz daoteke u listu:
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {
                string line;
                while ((line = reader.ReadLine()) != null) // čitanje svih linija iz dat.
                {
                    string parts = line; // razdvajanje linije na dijelove
                    
                    Pojmovi D = new Pojmovi(parts); // novi objekt
                    listaPojmova.Add(D); // umetanje objekta u listu
                }
                lb_Pojmovi.DataSource = null;
                lb_Pojmovi.DataSource = listaPojmova; // Prikaz svih elemenata liste u list box-u
            }
        }

        private void btn_nova_Click(object sender, EventArgs e)
        {
            broj_pokusaja = 10;
            int j;
            int i = rnd.Next(listaPojmova.Count);
            nasumicni = listaPojmova[i].ToString();

            for (j = 0; j < nasumicni.Length; j++) {
                lbl_crtice.Text = lbl_crtice.Text + "_  ";
            }
        }
    }
}
