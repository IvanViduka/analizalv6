﻿namespace WindowsFormsApp5
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lb_Pojmovi = new System.Windows.Forms.ListBox();
            this.btn_nova = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_broj = new System.Windows.Forms.Label();
            this.lbl_crtice = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lb_Pojmovi
            // 
            this.lb_Pojmovi.FormattingEnabled = true;
            this.lb_Pojmovi.Location = new System.Drawing.Point(12, 199);
            this.lb_Pojmovi.Name = "lb_Pojmovi";
            this.lb_Pojmovi.Size = new System.Drawing.Size(345, 212);
            this.lb_Pojmovi.TabIndex = 0;
            // 
            // btn_nova
            // 
            this.btn_nova.Location = new System.Drawing.Point(438, 388);
            this.btn_nova.Name = "btn_nova";
            this.btn_nova.Size = new System.Drawing.Size(75, 23);
            this.btn_nova.TabIndex = 1;
            this.btn_nova.Text = "Nova igra";
            this.btn_nova.UseVisualStyleBackColor = true;
            this.btn_nova.Click += new System.EventHandler(this.btn_nova_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Broj pokusaja:";
            // 
            // lbl_broj
            // 
            this.lbl_broj.AutoSize = true;
            this.lbl_broj.Location = new System.Drawing.Point(92, 33);
            this.lbl_broj.Name = "lbl_broj";
            this.lbl_broj.Size = new System.Drawing.Size(0, 13);
            this.lbl_broj.TabIndex = 3;
            this.lbl_broj.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lbl_crtice
            // 
            this.lbl_crtice.AutoSize = true;
            this.lbl_crtice.Location = new System.Drawing.Point(12, 170);
            this.lbl_crtice.Name = "lbl_crtice";
            this.lbl_crtice.Size = new System.Drawing.Size(0, 13);
            this.lbl_crtice.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lbl_crtice);
            this.Controls.Add(this.lbl_broj);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_nova);
            this.Controls.Add(this.lb_Pojmovi);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lb_Pojmovi;
        private System.Windows.Forms.Button btn_nova;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_broj;
        private System.Windows.Forms.Label lbl_crtice;
    }
}

