﻿namespace WindowsFormsApp4
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_2 = new System.Windows.Forms.Button();
            this.btn_3 = new System.Windows.Forms.Button();
            this.btn_1 = new System.Windows.Forms.Button();
            this.btn_6 = new System.Windows.Forms.Button();
            this.btn_0 = new System.Windows.Forms.Button();
            this.btn_7 = new System.Windows.Forms.Button();
            this.btn_9 = new System.Windows.Forms.Button();
            this.btn_5 = new System.Windows.Forms.Button();
            this.btn_8 = new System.Windows.Forms.Button();
            this.btn_4 = new System.Windows.Forms.Button();
            this.btn_dec = new System.Windows.Forms.Button();
            this.btn_div = new System.Windows.Forms.Button();
            this.btn_mul = new System.Windows.Forms.Button();
            this.btn_sub = new System.Windows.Forms.Button();
            this.btn_add = new System.Windows.Forms.Button();
            this.btn_clear = new System.Windows.Forms.Button();
            this.btn_percent = new System.Windows.Forms.Button();
            this.btn_sinus = new System.Windows.Forms.Button();
            this.btn_cos = new System.Windows.Forms.Button();
            this.btn_logaritam = new System.Windows.Forms.Button();
            this.btn_root = new System.Windows.Forms.Button();
            this.btn_equal = new System.Windows.Forms.Button();
            this.txtbox_unos = new System.Windows.Forms.TextBox();
            this.lbl_tren = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_2
            // 
            this.btn_2.Location = new System.Drawing.Point(110, 210);
            this.btn_2.Name = "btn_2";
            this.btn_2.Size = new System.Drawing.Size(40, 40);
            this.btn_2.TabIndex = 0;
            this.btn_2.Text = "2";
            this.btn_2.UseVisualStyleBackColor = true;
            this.btn_2.Click += new System.EventHandler(this.btn_2_Click);
            // 
            // btn_3
            // 
            this.btn_3.Location = new System.Drawing.Point(156, 210);
            this.btn_3.Name = "btn_3";
            this.btn_3.Size = new System.Drawing.Size(40, 40);
            this.btn_3.TabIndex = 1;
            this.btn_3.Text = "3";
            this.btn_3.UseVisualStyleBackColor = true;
            this.btn_3.Click += new System.EventHandler(this.btn_3_Click);
            // 
            // btn_1
            // 
            this.btn_1.Location = new System.Drawing.Point(64, 210);
            this.btn_1.Name = "btn_1";
            this.btn_1.Size = new System.Drawing.Size(40, 40);
            this.btn_1.TabIndex = 2;
            this.btn_1.Text = "1";
            this.btn_1.UseVisualStyleBackColor = true;
            this.btn_1.Click += new System.EventHandler(this.btn_1_Click);
            // 
            // btn_6
            // 
            this.btn_6.Location = new System.Drawing.Point(156, 164);
            this.btn_6.Name = "btn_6";
            this.btn_6.Size = new System.Drawing.Size(40, 40);
            this.btn_6.TabIndex = 3;
            this.btn_6.Text = "6";
            this.btn_6.UseVisualStyleBackColor = true;
            this.btn_6.Click += new System.EventHandler(this.btn_6_Click);
            // 
            // btn_0
            // 
            this.btn_0.Location = new System.Drawing.Point(64, 256);
            this.btn_0.Name = "btn_0";
            this.btn_0.Size = new System.Drawing.Size(86, 40);
            this.btn_0.TabIndex = 4;
            this.btn_0.Text = "0";
            this.btn_0.UseVisualStyleBackColor = true;
            this.btn_0.Click += new System.EventHandler(this.btn_0_Click);
            // 
            // btn_7
            // 
            this.btn_7.Location = new System.Drawing.Point(64, 118);
            this.btn_7.Name = "btn_7";
            this.btn_7.Size = new System.Drawing.Size(40, 40);
            this.btn_7.TabIndex = 5;
            this.btn_7.Text = "7";
            this.btn_7.UseVisualStyleBackColor = true;
            this.btn_7.Click += new System.EventHandler(this.btn_7_Click);
            // 
            // btn_9
            // 
            this.btn_9.Location = new System.Drawing.Point(156, 118);
            this.btn_9.Name = "btn_9";
            this.btn_9.Size = new System.Drawing.Size(40, 40);
            this.btn_9.TabIndex = 6;
            this.btn_9.Text = "9";
            this.btn_9.UseVisualStyleBackColor = true;
            this.btn_9.Click += new System.EventHandler(this.btn_9_Click);
            // 
            // btn_5
            // 
            this.btn_5.Location = new System.Drawing.Point(110, 164);
            this.btn_5.Name = "btn_5";
            this.btn_5.Size = new System.Drawing.Size(40, 40);
            this.btn_5.TabIndex = 7;
            this.btn_5.Text = "5";
            this.btn_5.UseVisualStyleBackColor = true;
            this.btn_5.Click += new System.EventHandler(this.btn_5_Click);
            // 
            // btn_8
            // 
            this.btn_8.Location = new System.Drawing.Point(110, 118);
            this.btn_8.Name = "btn_8";
            this.btn_8.Size = new System.Drawing.Size(40, 40);
            this.btn_8.TabIndex = 8;
            this.btn_8.Text = "8";
            this.btn_8.UseVisualStyleBackColor = true;
            this.btn_8.Click += new System.EventHandler(this.btn_8_Click);
            // 
            // btn_4
            // 
            this.btn_4.Location = new System.Drawing.Point(64, 164);
            this.btn_4.Name = "btn_4";
            this.btn_4.Size = new System.Drawing.Size(40, 40);
            this.btn_4.TabIndex = 9;
            this.btn_4.Text = "4";
            this.btn_4.UseVisualStyleBackColor = true;
            this.btn_4.Click += new System.EventHandler(this.btn_4_Click);
            // 
            // btn_dec
            // 
            this.btn_dec.Location = new System.Drawing.Point(156, 256);
            this.btn_dec.Name = "btn_dec";
            this.btn_dec.Size = new System.Drawing.Size(40, 40);
            this.btn_dec.TabIndex = 10;
            this.btn_dec.Text = ",";
            this.btn_dec.UseVisualStyleBackColor = true;
            this.btn_dec.Click += new System.EventHandler(this.btn_dot_Click);
            // 
            // btn_div
            // 
            this.btn_div.Location = new System.Drawing.Point(202, 256);
            this.btn_div.Name = "btn_div";
            this.btn_div.Size = new System.Drawing.Size(40, 40);
            this.btn_div.TabIndex = 11;
            this.btn_div.Text = "/";
            this.btn_div.UseVisualStyleBackColor = true;
            this.btn_div.Click += new System.EventHandler(this.btn_div_Click);
            // 
            // btn_mul
            // 
            this.btn_mul.Location = new System.Drawing.Point(202, 210);
            this.btn_mul.Name = "btn_mul";
            this.btn_mul.Size = new System.Drawing.Size(40, 40);
            this.btn_mul.TabIndex = 12;
            this.btn_mul.Text = "*";
            this.btn_mul.UseVisualStyleBackColor = true;
            this.btn_mul.Click += new System.EventHandler(this.btn_mul_Click);
            // 
            // btn_sub
            // 
            this.btn_sub.Location = new System.Drawing.Point(202, 164);
            this.btn_sub.Name = "btn_sub";
            this.btn_sub.Size = new System.Drawing.Size(40, 40);
            this.btn_sub.TabIndex = 13;
            this.btn_sub.Text = "-";
            this.btn_sub.UseVisualStyleBackColor = true;
            this.btn_sub.Click += new System.EventHandler(this.btn_sub_Click);
            // 
            // btn_add
            // 
            this.btn_add.Location = new System.Drawing.Point(202, 118);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(40, 40);
            this.btn_add.TabIndex = 14;
            this.btn_add.Text = "+";
            this.btn_add.UseVisualStyleBackColor = true;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // btn_clear
            // 
            this.btn_clear.Location = new System.Drawing.Point(294, 118);
            this.btn_clear.Name = "btn_clear";
            this.btn_clear.Size = new System.Drawing.Size(40, 40);
            this.btn_clear.TabIndex = 16;
            this.btn_clear.Text = "C";
            this.btn_clear.UseVisualStyleBackColor = true;
            this.btn_clear.Click += new System.EventHandler(this.btn_clear_Click);
            // 
            // btn_percent
            // 
            this.btn_percent.Location = new System.Drawing.Point(294, 164);
            this.btn_percent.Name = "btn_percent";
            this.btn_percent.Size = new System.Drawing.Size(40, 40);
            this.btn_percent.TabIndex = 17;
            this.btn_percent.Text = "%";
            this.btn_percent.UseVisualStyleBackColor = true;
            this.btn_percent.Click += new System.EventHandler(this.btn_percent_Click);
            // 
            // btn_sinus
            // 
            this.btn_sinus.Location = new System.Drawing.Point(248, 118);
            this.btn_sinus.Name = "btn_sinus";
            this.btn_sinus.Size = new System.Drawing.Size(40, 40);
            this.btn_sinus.TabIndex = 18;
            this.btn_sinus.Text = "sin";
            this.btn_sinus.UseVisualStyleBackColor = true;
            this.btn_sinus.Click += new System.EventHandler(this.btn_sinus_Click);
            // 
            // btn_cos
            // 
            this.btn_cos.Location = new System.Drawing.Point(248, 164);
            this.btn_cos.Name = "btn_cos";
            this.btn_cos.Size = new System.Drawing.Size(40, 40);
            this.btn_cos.TabIndex = 19;
            this.btn_cos.Text = "cos";
            this.btn_cos.UseVisualStyleBackColor = true;
            this.btn_cos.Click += new System.EventHandler(this.btn_cos_Click);
            // 
            // btn_logaritam
            // 
            this.btn_logaritam.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.7F);
            this.btn_logaritam.Location = new System.Drawing.Point(248, 210);
            this.btn_logaritam.Name = "btn_logaritam";
            this.btn_logaritam.Size = new System.Drawing.Size(40, 40);
            this.btn_logaritam.TabIndex = 20;
            this.btn_logaritam.Text = "log10";
            this.btn_logaritam.UseVisualStyleBackColor = true;
            this.btn_logaritam.Click += new System.EventHandler(this.btn_logaritam_Click);
            // 
            // btn_root
            // 
            this.btn_root.Location = new System.Drawing.Point(248, 256);
            this.btn_root.Name = "btn_root";
            this.btn_root.Size = new System.Drawing.Size(40, 40);
            this.btn_root.TabIndex = 21;
            this.btn_root.Text = "sqrt";
            this.btn_root.UseVisualStyleBackColor = true;
            this.btn_root.Click += new System.EventHandler(this.btn_root_Click);
            // 
            // btn_equal
            // 
            this.btn_equal.Location = new System.Drawing.Point(294, 210);
            this.btn_equal.Name = "btn_equal";
            this.btn_equal.Size = new System.Drawing.Size(40, 86);
            this.btn_equal.TabIndex = 22;
            this.btn_equal.Text = "=";
            this.btn_equal.UseVisualStyleBackColor = true;
            this.btn_equal.Click += new System.EventHandler(this.btn_equal_Click);
            // 
            // txtbox_unos
            // 
            this.txtbox_unos.Location = new System.Drawing.Point(64, 80);
            this.txtbox_unos.Name = "txtbox_unos";
            this.txtbox_unos.Size = new System.Drawing.Size(270, 20);
            this.txtbox_unos.TabIndex = 23;
            this.txtbox_unos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lbl_tren
            // 
            this.lbl_tren.AutoSize = true;
            this.lbl_tren.Location = new System.Drawing.Point(304, 64);
            this.lbl_tren.Name = "lbl_tren";
            this.lbl_tren.Size = new System.Drawing.Size(0, 13);
            this.lbl_tren.TabIndex = 24;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(355, 320);
            this.Controls.Add(this.lbl_tren);
            this.Controls.Add(this.txtbox_unos);
            this.Controls.Add(this.btn_equal);
            this.Controls.Add(this.btn_root);
            this.Controls.Add(this.btn_logaritam);
            this.Controls.Add(this.btn_cos);
            this.Controls.Add(this.btn_sinus);
            this.Controls.Add(this.btn_percent);
            this.Controls.Add(this.btn_clear);
            this.Controls.Add(this.btn_add);
            this.Controls.Add(this.btn_sub);
            this.Controls.Add(this.btn_mul);
            this.Controls.Add(this.btn_div);
            this.Controls.Add(this.btn_dec);
            this.Controls.Add(this.btn_4);
            this.Controls.Add(this.btn_8);
            this.Controls.Add(this.btn_5);
            this.Controls.Add(this.btn_9);
            this.Controls.Add(this.btn_7);
            this.Controls.Add(this.btn_0);
            this.Controls.Add(this.btn_6);
            this.Controls.Add(this.btn_1);
            this.Controls.Add(this.btn_3);
            this.Controls.Add(this.btn_2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_2;
        private System.Windows.Forms.Button btn_3;
        private System.Windows.Forms.Button btn_1;
        private System.Windows.Forms.Button btn_6;
        private System.Windows.Forms.Button btn_0;
        private System.Windows.Forms.Button btn_7;
        private System.Windows.Forms.Button btn_9;
        private System.Windows.Forms.Button btn_5;
        private System.Windows.Forms.Button btn_8;
        private System.Windows.Forms.Button btn_4;
        private System.Windows.Forms.Button btn_dec;
        private System.Windows.Forms.Button btn_div;
        private System.Windows.Forms.Button btn_mul;
        private System.Windows.Forms.Button btn_sub;
        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.Button btn_clear;
        private System.Windows.Forms.Button btn_percent;
        private System.Windows.Forms.Button btn_sinus;
        private System.Windows.Forms.Button btn_cos;
        private System.Windows.Forms.Button btn_logaritam;
        private System.Windows.Forms.Button btn_root;
        private System.Windows.Forms.Button btn_equal;
        private System.Windows.Forms.TextBox txtbox_unos;
        private System.Windows.Forms.Label lbl_tren;
    }
}

