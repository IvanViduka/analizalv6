﻿namespace WindowsFormsApp4
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_2 = new System.Windows.Forms.Button();
            this.btn_3 = new System.Windows.Forms.Button();
            this.btn_1 = new System.Windows.Forms.Button();
            this.btn_6 = new System.Windows.Forms.Button();
            this.btn_0 = new System.Windows.Forms.Button();
            this.btn_7 = new System.Windows.Forms.Button();
            this.btn_9 = new System.Windows.Forms.Button();
            this.btn_5 = new System.Windows.Forms.Button();
            this.btn_8 = new System.Windows.Forms.Button();
            this.btn_4 = new System.Windows.Forms.Button();
            this.btn_dot = new System.Windows.Forms.Button();
            this.btn_div = new System.Windows.Forms.Button();
            this.btn_mul = new System.Windows.Forms.Button();
            this.btn_sub = new System.Windows.Forms.Button();
            this.btn_add = new System.Windows.Forms.Button();
            this.btn_erase = new System.Windows.Forms.Button();
            this.btn_clear = new System.Windows.Forms.Button();
            this.btn_percent = new System.Windows.Forms.Button();
            this.btn_sinus = new System.Windows.Forms.Button();
            this.btn_cos = new System.Windows.Forms.Button();
            this.btn_logaritam = new System.Windows.Forms.Button();
            this.btn_root = new System.Windows.Forms.Button();
            this.btn_equal = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_2
            // 
            this.btn_2.Location = new System.Drawing.Point(110, 210);
            this.btn_2.Name = "btn_2";
            this.btn_2.Size = new System.Drawing.Size(40, 40);
            this.btn_2.TabIndex = 0;
            this.btn_2.Text = "2";
            this.btn_2.UseVisualStyleBackColor = true;
            // 
            // btn_3
            // 
            this.btn_3.Location = new System.Drawing.Point(156, 210);
            this.btn_3.Name = "btn_3";
            this.btn_3.Size = new System.Drawing.Size(40, 40);
            this.btn_3.TabIndex = 1;
            this.btn_3.Text = "3";
            this.btn_3.UseVisualStyleBackColor = true;
            // 
            // btn_1
            // 
            this.btn_1.Location = new System.Drawing.Point(64, 210);
            this.btn_1.Name = "btn_1";
            this.btn_1.Size = new System.Drawing.Size(40, 40);
            this.btn_1.TabIndex = 2;
            this.btn_1.Text = "1";
            this.btn_1.UseVisualStyleBackColor = true;
            // 
            // btn_6
            // 
            this.btn_6.Location = new System.Drawing.Point(156, 164);
            this.btn_6.Name = "btn_6";
            this.btn_6.Size = new System.Drawing.Size(40, 40);
            this.btn_6.TabIndex = 3;
            this.btn_6.Text = "6";
            this.btn_6.UseVisualStyleBackColor = true;
            // 
            // btn_0
            // 
            this.btn_0.Location = new System.Drawing.Point(64, 256);
            this.btn_0.Name = "btn_0";
            this.btn_0.Size = new System.Drawing.Size(86, 40);
            this.btn_0.TabIndex = 4;
            this.btn_0.Text = "0";
            this.btn_0.UseVisualStyleBackColor = true;
            // 
            // btn_7
            // 
            this.btn_7.Location = new System.Drawing.Point(64, 118);
            this.btn_7.Name = "btn_7";
            this.btn_7.Size = new System.Drawing.Size(40, 40);
            this.btn_7.TabIndex = 5;
            this.btn_7.Text = "7";
            this.btn_7.UseVisualStyleBackColor = true;
            // 
            // btn_9
            // 
            this.btn_9.Location = new System.Drawing.Point(156, 118);
            this.btn_9.Name = "btn_9";
            this.btn_9.Size = new System.Drawing.Size(40, 40);
            this.btn_9.TabIndex = 6;
            this.btn_9.Text = "9";
            this.btn_9.UseVisualStyleBackColor = true;
            // 
            // btn_5
            // 
            this.btn_5.Location = new System.Drawing.Point(110, 164);
            this.btn_5.Name = "btn_5";
            this.btn_5.Size = new System.Drawing.Size(40, 40);
            this.btn_5.TabIndex = 7;
            this.btn_5.Text = "5";
            this.btn_5.UseVisualStyleBackColor = true;
            // 
            // btn_8
            // 
            this.btn_8.Location = new System.Drawing.Point(110, 118);
            this.btn_8.Name = "btn_8";
            this.btn_8.Size = new System.Drawing.Size(40, 40);
            this.btn_8.TabIndex = 8;
            this.btn_8.Text = "8";
            this.btn_8.UseVisualStyleBackColor = true;
            // 
            // btn_4
            // 
            this.btn_4.Location = new System.Drawing.Point(64, 164);
            this.btn_4.Name = "btn_4";
            this.btn_4.Size = new System.Drawing.Size(40, 40);
            this.btn_4.TabIndex = 9;
            this.btn_4.Text = "4";
            this.btn_4.UseVisualStyleBackColor = true;
            // 
            // btn_dot
            // 
            this.btn_dot.Location = new System.Drawing.Point(156, 256);
            this.btn_dot.Name = "btn_dot";
            this.btn_dot.Size = new System.Drawing.Size(40, 40);
            this.btn_dot.TabIndex = 10;
            this.btn_dot.Text = ".";
            this.btn_dot.UseVisualStyleBackColor = true;
            // 
            // btn_div
            // 
            this.btn_div.Location = new System.Drawing.Point(202, 256);
            this.btn_div.Name = "btn_div";
            this.btn_div.Size = new System.Drawing.Size(40, 40);
            this.btn_div.TabIndex = 11;
            this.btn_div.Text = "/";
            this.btn_div.UseVisualStyleBackColor = true;
            // 
            // btn_mul
            // 
            this.btn_mul.Location = new System.Drawing.Point(202, 210);
            this.btn_mul.Name = "btn_mul";
            this.btn_mul.Size = new System.Drawing.Size(40, 40);
            this.btn_mul.TabIndex = 12;
            this.btn_mul.Text = "*";
            this.btn_mul.UseVisualStyleBackColor = true;
            // 
            // btn_sub
            // 
            this.btn_sub.Location = new System.Drawing.Point(202, 164);
            this.btn_sub.Name = "btn_sub";
            this.btn_sub.Size = new System.Drawing.Size(40, 40);
            this.btn_sub.TabIndex = 13;
            this.btn_sub.Text = "-";
            this.btn_sub.UseVisualStyleBackColor = true;
            // 
            // btn_add
            // 
            this.btn_add.Location = new System.Drawing.Point(202, 118);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(40, 40);
            this.btn_add.TabIndex = 14;
            this.btn_add.Text = "+";
            this.btn_add.UseVisualStyleBackColor = true;
            // 
            // btn_erase
            // 
            this.btn_erase.Location = new System.Drawing.Point(294, 118);
            this.btn_erase.Name = "btn_erase";
            this.btn_erase.Size = new System.Drawing.Size(40, 40);
            this.btn_erase.TabIndex = 15;
            this.btn_erase.Text = "<-";
            this.btn_erase.UseVisualStyleBackColor = true;
            // 
            // btn_clear
            // 
            this.btn_clear.Location = new System.Drawing.Point(294, 164);
            this.btn_clear.Name = "btn_clear";
            this.btn_clear.Size = new System.Drawing.Size(40, 40);
            this.btn_clear.TabIndex = 16;
            this.btn_clear.Text = "CE";
            this.btn_clear.UseVisualStyleBackColor = true;
            // 
            // btn_percent
            // 
            this.btn_percent.Location = new System.Drawing.Point(294, 210);
            this.btn_percent.Name = "btn_percent";
            this.btn_percent.Size = new System.Drawing.Size(40, 40);
            this.btn_percent.TabIndex = 17;
            this.btn_percent.Text = "%";
            this.btn_percent.UseVisualStyleBackColor = true;
            // 
            // btn_sinus
            // 
            this.btn_sinus.Location = new System.Drawing.Point(248, 118);
            this.btn_sinus.Name = "btn_sinus";
            this.btn_sinus.Size = new System.Drawing.Size(40, 40);
            this.btn_sinus.TabIndex = 18;
            this.btn_sinus.Text = "sin";
            this.btn_sinus.UseVisualStyleBackColor = true;
            // 
            // btn_cos
            // 
            this.btn_cos.Location = new System.Drawing.Point(248, 164);
            this.btn_cos.Name = "btn_cos";
            this.btn_cos.Size = new System.Drawing.Size(40, 40);
            this.btn_cos.TabIndex = 19;
            this.btn_cos.Text = "cos";
            this.btn_cos.UseVisualStyleBackColor = true;
            // 
            // btn_logaritam
            // 
            this.btn_logaritam.Location = new System.Drawing.Point(248, 210);
            this.btn_logaritam.Name = "btn_logaritam";
            this.btn_logaritam.Size = new System.Drawing.Size(40, 40);
            this.btn_logaritam.TabIndex = 20;
            this.btn_logaritam.Text = "log";
            this.btn_logaritam.UseVisualStyleBackColor = true;
            // 
            // btn_root
            // 
            this.btn_root.Location = new System.Drawing.Point(248, 256);
            this.btn_root.Name = "btn_root";
            this.btn_root.Size = new System.Drawing.Size(40, 40);
            this.btn_root.TabIndex = 21;
            this.btn_root.Text = "sqrt";
            this.btn_root.UseVisualStyleBackColor = true;
            // 
            // btn_equal
            // 
            this.btn_equal.Location = new System.Drawing.Point(294, 256);
            this.btn_equal.Name = "btn_equal";
            this.btn_equal.Size = new System.Drawing.Size(40, 40);
            this.btn_equal.TabIndex = 22;
            this.btn_equal.Text = "=";
            this.btn_equal.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(472, 452);
            this.Controls.Add(this.btn_equal);
            this.Controls.Add(this.btn_root);
            this.Controls.Add(this.btn_logaritam);
            this.Controls.Add(this.btn_cos);
            this.Controls.Add(this.btn_sinus);
            this.Controls.Add(this.btn_percent);
            this.Controls.Add(this.btn_clear);
            this.Controls.Add(this.btn_erase);
            this.Controls.Add(this.btn_add);
            this.Controls.Add(this.btn_sub);
            this.Controls.Add(this.btn_mul);
            this.Controls.Add(this.btn_div);
            this.Controls.Add(this.btn_dot);
            this.Controls.Add(this.btn_4);
            this.Controls.Add(this.btn_8);
            this.Controls.Add(this.btn_5);
            this.Controls.Add(this.btn_9);
            this.Controls.Add(this.btn_7);
            this.Controls.Add(this.btn_0);
            this.Controls.Add(this.btn_6);
            this.Controls.Add(this.btn_1);
            this.Controls.Add(this.btn_3);
            this.Controls.Add(this.btn_2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_2;
        private System.Windows.Forms.Button btn_3;
        private System.Windows.Forms.Button btn_1;
        private System.Windows.Forms.Button btn_6;
        private System.Windows.Forms.Button btn_0;
        private System.Windows.Forms.Button btn_7;
        private System.Windows.Forms.Button btn_9;
        private System.Windows.Forms.Button btn_5;
        private System.Windows.Forms.Button btn_8;
        private System.Windows.Forms.Button btn_4;
        private System.Windows.Forms.Button btn_dot;
        private System.Windows.Forms.Button btn_div;
        private System.Windows.Forms.Button btn_mul;
        private System.Windows.Forms.Button btn_sub;
        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.Button btn_erase;
        private System.Windows.Forms.Button btn_clear;
        private System.Windows.Forms.Button btn_percent;
        private System.Windows.Forms.Button btn_sinus;
        private System.Windows.Forms.Button btn_cos;
        private System.Windows.Forms.Button btn_logaritam;
        private System.Windows.Forms.Button btn_root;
        private System.Windows.Forms.Button btn_equal;
    }
}

