﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp4
{
    public partial class Form1 : Form
    {
        double rezultat = 0;
        string operacija = "";
        bool prva = true;
        bool prva_jednako = true;
        double zadnja_vrijednost = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void btn_0_Click(object sender, EventArgs e)
        {
            txtbox_unos.Text = txtbox_unos.Text + "0";
        }

        private void btn_1_Click(object sender, EventArgs e)
        {
            txtbox_unos.Text = txtbox_unos.Text + "1";
        }

        private void btn_2_Click(object sender, EventArgs e)
        {
            txtbox_unos.Text = txtbox_unos.Text + "2";
        }

        private void btn_3_Click(object sender, EventArgs e)
        {
            txtbox_unos.Text = txtbox_unos.Text + "3";
        }

        private void btn_4_Click(object sender, EventArgs e)
        {
            txtbox_unos.Text = txtbox_unos.Text + "4";
        }

        private void btn_5_Click(object sender, EventArgs e)
        {
            txtbox_unos.Text = txtbox_unos.Text + "5";
        }

        private void btn_6_Click(object sender, EventArgs e)
        {
            txtbox_unos.Text = txtbox_unos.Text + "6";
        }

        private void btn_7_Click(object sender, EventArgs e)
        {
            txtbox_unos.Text = txtbox_unos.Text + "7";
        }

        private void btn_8_Click(object sender, EventArgs e)
        {
            txtbox_unos.Text = txtbox_unos.Text + "8";
        }

        private void btn_9_Click(object sender, EventArgs e)
        {
            txtbox_unos.Text = txtbox_unos.Text + "9";
        }

        private void btn_dot_Click(object sender, EventArgs e)
        {
            txtbox_unos.Text = txtbox_unos.Text + ",";
        }

        private void btn_clear_Click(object sender, EventArgs e)
        {
            rezultat = 0;
            txtbox_unos.Text = "";
            operacija = "";
            prva = true;
            prva_jednako = true;
            zadnja_vrijednost = 0;
            lbl_tren.Text = "";
        }



        private void btn_add_Click(object sender, EventArgs e)
        {
            if (!prva_jednako)
            {
                prva_jednako = true;
                operacija = "+";
                txtbox_unos.Text = "";
            }
            else
            {

                rezultat += Double.Parse(txtbox_unos.Text);
                operacija = "+";
                txtbox_unos.Text = "";
                prva = false;
                prva_jednako = true;
                lbl_tren.Text = rezultat.ToString();

            }
        }



        private void btn_sub_Click(object sender, EventArgs e)
        {
            if (!prva_jednako)
            {
                prva_jednako = true;
                operacija = "-";
                txtbox_unos.Text = "";
            }


            else if (prva)
            {
                prva_jednako = true;
                rezultat += Double.Parse(txtbox_unos.Text);
                prva = false;
                lbl_tren.Text = rezultat.ToString();
                operacija = "-";
                txtbox_unos.Text = "";
            }
            else
            {
                prva_jednako = true;
                rezultat -= Double.Parse(txtbox_unos.Text);
                operacija = "-";
                txtbox_unos.Text = "";
                lbl_tren.Text = rezultat.ToString();
            }
           
        }
        
        private void btn_mul_Click(object sender, EventArgs e)
        {
            if (!prva_jednako)
            {
                prva_jednako = true;
                operacija = "*";
                txtbox_unos.Text = "";
            }

            else if (prva)
            {
                prva_jednako = true;
                rezultat += Double.Parse(txtbox_unos.Text);
                prva = false;
                lbl_tren.Text = rezultat.ToString();
                operacija = "*";
                txtbox_unos.Text = "";
            }
            else
            {
                prva_jednako = true;
                rezultat *= Double.Parse(txtbox_unos.Text);
                operacija = "*";
                txtbox_unos.Text = "";
                lbl_tren.Text = rezultat.ToString();
            }
        }

        private void btn_div_Click(object sender, EventArgs e)
        {
            if (!prva_jednako)
            {
                prva_jednako = true;
                operacija = "/";
                txtbox_unos.Text = "";
            }

            else if (prva){
                prva_jednako = true;
                rezultat += Double.Parse(txtbox_unos.Text);
                operacija = "/";
                prva = false;
                lbl_tren.Text = rezultat.ToString();
                txtbox_unos.Text = "";
            }


            else if (Double.Parse(txtbox_unos.Text) == 0)
                MessageBox.Show("Ne moze se dijeliti s 0", "Pogreška");

            else
            {
                prva_jednako = true;
                rezultat /= Double.Parse(txtbox_unos.Text);
                operacija = "/";
                txtbox_unos.Text = "";
                lbl_tren.Text = rezultat.ToString();
            }
        }

        private void btn_equal_Click(object sender, EventArgs e)
        {

            if (operacija == "+")
            {
                if (prva_jednako)
                {
                    prva_jednako = false;
                  
                    zadnja_vrijednost = double.Parse(txtbox_unos.Text);
                    txtbox_unos.Text = (rezultat + double.Parse(txtbox_unos.Text)).ToString();
                    rezultat = Double.Parse(txtbox_unos.Text);
                    lbl_tren.Text = "";
                }

                else {
                    txtbox_unos.Text = (zadnja_vrijednost + double.Parse(txtbox_unos.Text)).ToString();
                    rezultat = Double.Parse(txtbox_unos.Text);
                    lbl_tren.Text = "";
                }
                
            }
            else if (operacija == "-")
            {
                if (prva_jednako)
                {
                    prva_jednako = false;
                    
                    zadnja_vrijednost = double.Parse(txtbox_unos.Text);
                    txtbox_unos.Text = (rezultat - double.Parse(txtbox_unos.Text)).ToString();
                    rezultat = Double.Parse(txtbox_unos.Text);
                    lbl_tren.Text = "";
                }

                else
                {
                    txtbox_unos.Text = (double.Parse(txtbox_unos.Text)- zadnja_vrijednost).ToString();
                    rezultat = Double.Parse(txtbox_unos.Text);
                    lbl_tren.Text = "";
                }

            }
            else if (operacija == "*")
            {
                if (prva_jednako)
                {
                    prva_jednako = false;
                    
                    zadnja_vrijednost = double.Parse(txtbox_unos.Text);
                    txtbox_unos.Text = (rezultat * double.Parse(txtbox_unos.Text)).ToString();
                    rezultat = Double.Parse(txtbox_unos.Text);
                    lbl_tren.Text = "";
                }

                else
                {
                    txtbox_unos.Text = (zadnja_vrijednost * double.Parse(txtbox_unos.Text)).ToString();
                    rezultat = Double.Parse(txtbox_unos.Text);
                    lbl_tren.Text = "";
                }


            }
            else if (operacija == "/")
            {
                if (Double.Parse(txtbox_unos.Text) == 0)
                    MessageBox.Show("Ne moze se dijeliti s 0", "Pogreška");

                else if (prva_jednako)
                {
                    prva_jednako = false;
                    
                    zadnja_vrijednost = double.Parse(txtbox_unos.Text);
                    txtbox_unos.Text = (rezultat / double.Parse(txtbox_unos.Text)).ToString();
                    rezultat = Double.Parse(txtbox_unos.Text);
                    lbl_tren.Text = "";
                }

                else
                {
                    txtbox_unos.Text = ( double.Parse(txtbox_unos.Text) / zadnja_vrijednost).ToString();
                    rezultat = Double.Parse(txtbox_unos.Text);
                    lbl_tren.Text = "";

                }
            }    


        }

        private void btn_sinus_Click(object sender, EventArgs e)
        {
            txtbox_unos.Text = Math.Sin(double.Parse(txtbox_unos.Text)).ToString();
            lbl_tren.Text = "";
        }

        private void btn_cos_Click(object sender, EventArgs e)
        {
            txtbox_unos.Text = Math.Cos(double.Parse(txtbox_unos.Text)).ToString();
            lbl_tren.Text = "";
        }

        private void btn_logaritam_Click(object sender, EventArgs e)
        {
            if(double.Parse(txtbox_unos.Text) <= 0)
                MessageBox.Show("Unesite pozitivnu vrijednost", "Pogreska");

            else
            {

                txtbox_unos.Text = Math.Log10(double.Parse(txtbox_unos.Text)).ToString();
                lbl_tren.Text = "";
            }
        }

        private void btn_root_Click(object sender, EventArgs e)
        {
            if (double.Parse(txtbox_unos.Text) < 0)
                MessageBox.Show("Negativna vrijednost ispod korijena", "Pogreska");

            else
            {

                txtbox_unos.Text = Math.Sqrt(double.Parse(txtbox_unos.Text)).ToString();
                lbl_tren.Text = "";
            }
        }

        private void btn_percent_Click(object sender, EventArgs e)
        {
            txtbox_unos.Text = (double.Parse(txtbox_unos.Text)*100).ToString();
            lbl_tren.Text = "";
        }
    }
}

